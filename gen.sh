#!/usr/bin/env sh
# coding: UTF-8

# Sheep for GIF with ImageMagick and *nix Shell
# 
# Depends: ImageMagick and *nix Shell
# License: WTFPL

convert -size 120x120 \
        "xc:#63FF63" \
        -fill "#6C6CFF" -draw "rectangle 0,0,120,50" \
        fence.png -geometry "+34+42" -composite \
        base.png

i=0
for x in `seq -20 5 140`; do

    y=35
    if [ $x -eq 40 -o $x -eq 65 ]; then
        y=38
    elif [ $x -eq 45 -o $x -eq 60 ]; then
        y=41
    elif [ $x -eq 50 -o $x -eq 55 ]; then
        y=44
    fi

    i=`dc -e "$i 1 + p"`
    convert base.png "sheep0`dc -e "$x 2 % p"`.png" \
            -gravity southeast -geometry "+$x+$y" -composite \
            "sheep_anim$i.png"

done

convert -delay 10 -loop 0 sheep_anim*.png sheep.gif

# rm base.png
# rm sheep_anim*.png

